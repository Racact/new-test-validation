using Microsoft.CSharp;
using Racact.CreditImmo.Core.Enums;
using Racact.CreditImmo.Core.Models;
using Racact.CreditImmo.Core.Services;

namespace test
{
    public class CreditImmoUnitTest
    {
        private readonly FinancialData _financialData = new FinancialData();
        private readonly CalculatorService _calculator = new CalculatorService();

        [Theory]
        [InlineData(-200)]
        [InlineData(0)]
        [InlineData(49990)]
        public void InputCapitalAmountIsLessThan50000Exception(double inputCapitalAmount)
        {
            _financialData.CapitalAmount = inputCapitalAmount;
           
            Assert.Throws<ArithmeticException>(() => _calculator.MonthlyPaymentWithoutInsurance(_financialData));
        }

        [Theory]
        [InlineData(-5)]
        [InlineData(0)]
        [InlineData(8)]
        [InlineData(26)]
        public void DurationIsNotInBetween9And25YearsException(double inputDurationByYears)
        {
            _financialData.CapitalAmount = 52000;
            _financialData.DurationByYears = inputDurationByYears;

            Assert.Throws<ArithmeticException>(() => _calculator.MonthlyPaymentWithoutInsurance(_financialData));
        }

        [Theory]
        [InlineData(ERateType.Good, ELoanDurationRange.Seven, 0.0256)]
        [InlineData(ERateType.VeryGood, ELoanDurationRange.Ten, 0.0247)]
        [InlineData(ERateType.Excellent, ELoanDurationRange.Fifteen, 0.0233)]
        public void AnnualNominalRateInArrayIsEqual(ERateType inputRateType, ELoanDurationRange inputLoanDurationRange, double expectedRate)
        {
            _financialData.RateType = inputRateType;
            _financialData.LoanDurationRange = inputLoanDurationRange;

            double rateCalculated = _calculator.AnnualNominalRate(_financialData);

            Assert.Equal(expectedRate, rateCalculated);
        }

        [Theory]
        [InlineData(ECaracteristic.Smoker, ECaracteristic.Cardiac, 0.0075)]
        [InlineData(ECaracteristic.Engineer, ECaracteristic.Sportsman, 0.002)]
        [InlineData(ECaracteristic.Pilot, ECaracteristic.Sportsman, 0.004)]
        public void InsuranceRateIsEqual(ECaracteristic inputCaracteristicFirst, ECaracteristic inputCaracteristicSecond, double expectedInsuranceRate)
        {
            _financialData.Caracteristics.Add(inputCaracteristicFirst);
            _financialData.Caracteristics.Add(inputCaracteristicSecond);

            double rateCalculated = _calculator.InsuranceRate(_financialData);

            Assert.Equal(expectedInsuranceRate, rateCalculated);
        }

    }
}