﻿using Racact.CreditImmo.Core.Enums;
using Racact.CreditImmo.Core.Exceptions;
using Racact.CreditImmo.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Racact.CreditImmo.Core.Services
{
    public class CalculatorService
    {
        private readonly double[,] _annualNominalRates = new double[3,5]
        {
            { 0.0256, 0.0263, 0.0279, 0.029, 0.03 },
            { 0.0237, 0.0247, 0.0267, 0.0276, 0.0288},
            { 0.021, 0.0215, 0.0233, 0.0233, 0.0245 }
        };

        public double MonthlyPaymentWithoutInsurance(FinancialData financialData)
        {
            FinancialDataException financialDataException = new FinancialDataException();
            financialDataException.ExceptionAmountLessThan50000(financialData.CapitalAmount);
            financialDataException.ExceptionDurationByYearsIsNotBetween9and25(financialData.DurationByYears);

            return financialData.CapitalAmount;
        }

        public double AnnualNominalRate(FinancialData financialData)
        {
            LoanDurationRangeCorresponding(financialData);
            financialData.AnnualNominalRate = _annualNominalRates[(int)financialData.RateType , (int)financialData.LoanDurationRange];

            return financialData.AnnualNominalRate;
        }

        public void LoanDurationRangeCorresponding(FinancialData financialData)
        {
            switch(financialData.DurationByYears)
            {
                case >= 7 and < 10:
                    financialData.LoanDurationRange = ELoanDurationRange.Seven;
                    break;
                case >= 10 and < 15:
                    financialData.LoanDurationRange = ELoanDurationRange.Ten;
                    break;
                case >= 15 and < 20:
                    financialData.LoanDurationRange = ELoanDurationRange.Fifteen;
                    break;
                case >= 20 and < 25:
                    financialData.LoanDurationRange = ELoanDurationRange.Twenty;
                    break;
                case 25:
                    financialData.LoanDurationRange = ELoanDurationRange.TwentyFive;
                    break;
                default: break;
            }
        }
    }
}
