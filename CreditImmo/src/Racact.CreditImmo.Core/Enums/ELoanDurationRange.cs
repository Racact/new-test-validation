﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Racact.CreditImmo.Core.Enums
{
    public enum ELoanDurationRange
    {
        Seven,
        Ten,
        Fifteen,
        Twenty,
        TwentyFive
    }
}
