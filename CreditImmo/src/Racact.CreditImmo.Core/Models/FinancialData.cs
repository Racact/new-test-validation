﻿using Racact.CreditImmo.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Racact.CreditImmo.Core.Models
{
    public class FinancialData
    {
        public double CapitalAmount { get; set; }
        public double DurationByYears { get; set; }
        public ELoanDurationRange LoanDurationRange { get; set; }
        public ERateType RateType { get; set; }
        public double AnnualNominalRate { get; set; }

    }
}
