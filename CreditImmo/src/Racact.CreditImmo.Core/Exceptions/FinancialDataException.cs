﻿using Racact.CreditImmo.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Racact.CreditImmo.Core.Exceptions
{
    public class FinancialDataException
    {
        public void ExceptionAmountLessThan50000(double inputCapitalAmount)
        {
            if (inputCapitalAmount < 50000)
                throw new ArithmeticException("Error input amount less than 50000");
        }

        public void ExceptionDurationByYearsIsNotBetween9and25(double inputDurationByYears)
        {
            if (inputDurationByYears < 9 || inputDurationByYears > 25)
                throw new ArithmeticException("Error input years of real-estate loan duration must be between 9 and 25 years");
        }
    }
}
